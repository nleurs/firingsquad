package automata;

import java.util.Vector;

public class Population extends Vector<Solution> {
    public Population(Vector<Solution> population) {
        addAll(population);
    }

    public Population(Solution ... solutions) {
        for(Solution sol : solutions)
            add(sol);
    }
}
