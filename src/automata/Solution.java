package automata;

import java.util.Random;
import java.util.Vector;

public class Solution {
    private int[] rules;
    private int fitness;

    public Solution(int nbRules) {
        rules = new int[nbRules];
    }

    public Solution(int nbRules, int fitness) {
        this(nbRules);
        this.fitness = fitness;
    }

    public Solution(Solution solution) {
        rules = new int[solution.rules.length];
        copySolution(solution);
    }

    public void copySolution(Solution solution) {
        System.arraycopy(solution.rules, 0, rules, 0, solution.rules.length);
        fitness = solution.fitness;
    }

    @Override
    public String toString() {
        String tab = new String();
        for (int i = 0; i < this.rules.length; i++) {
            tab += this.rules[i] + " ";
        }
        return "Fitness = " + this.fitness + "\n" + "Solution : " + tab;
    }

    public int[] getRules() {
        return rules;
    }

    public int getFitness() {
        return fitness;
    }

    public int getRuleAt(Integer i) {
        return rules[i];
    }

    public void setRuleAt(Integer i, int value) {
        rules[i] = value;
    }

    public void setFitness(int fitness) {
        this.fitness = fitness;
    }

    public void getNeighbour(Solution solution) {
        Random generator;
        generator = new Random();
        Solution neighbour = new Solution(solution);
        boolean equal = true;
        Initialization initialization = new Initialization();
        int sizeMax = 20;
        Automata automate = new Automata(sizeMax);

        while (equal) {
            neighbour.copySolution(solution);
            int i = generator.nextInt(solution.rules.length);
            int r = generator.nextInt(3);
            neighbour.setRuleAt(i, r);
            initialization.init(neighbour);
            if (neighbour.getRuleAt(i) != solution.getRuleAt(i)) {
                neighbour.setFitness(automate.f(neighbour.getRules(), sizeMax));
                if (neighbour.fitness >= solution.fitness) {
                    equal = false;
                }
            }
        }


        //System.out.println(neighbour.toString());
        solution.copySolution(neighbour);
    }

    public void iterate(Solution solution){
        Random generator;
        generator = new Random();
        Solution neighbour = new Solution(solution);
        boolean equal = true;
        Initialization initialization = new Initialization();
        int sizeMax = 20;
        Automata automate = new Automata(sizeMax);
        int ite=0;

        while (equal) {
            if(ite>2)
            {
                solution.perturbation(solution);
                ite=0;
            }
            else{
                ite++;
            }
            neighbour.copySolution(solution);
            int i = generator.nextInt(solution.rules.length);
            int r = generator.nextInt(3);
            neighbour.setRuleAt(i, r);
            initialization.init(neighbour);
            if (neighbour.getRuleAt(i) != solution.getRuleAt(i)) {
                neighbour.setFitness(automate.f(neighbour.getRules(), sizeMax));
                if (neighbour.fitness > solution.fitness) {
                    equal = false;
                }
            }
        }


        //System.out.println(neighbour.toString());
        solution.copySolution(neighbour);
    }
    public void perturbation(Solution solution) {
        Random generator;
        generator = new Random();
        Solution s = new Solution(solution);
        boolean equal = true;
        Initialization initialization = new Initialization();
        int sizeMax = 20;
        Automata automate = new Automata(sizeMax);
        int temp=0;

        while (equal && temp>3) {
            int i = generator.nextInt(solution.rules.length);
            int r = generator.nextInt(3);
            s.setRuleAt(i, r);
            initialization.init(s);
            if (s.getRuleAt(i) != solution.getRuleAt(i)) {
                s.setFitness(automate.f(s.getRules(), sizeMax));
                if (s.fitness >= solution.fitness) {
                    equal = false;
                    temp++;
                }
            }
        }
        solution.copySolution(s);
    }

    public void recuitSimule(Solution solution){
        Random generator;
        generator = new Random();
        Solution neighbour = new Solution(solution);
        boolean equal = true;
        Initialization initialization = new Initialization();
        int sizeMax = 20;
        Automata automate = new Automata(sizeMax);

        while (equal) {
            int i = generator.nextInt(solution.rules.length);
            int r = generator.nextInt(3);
            neighbour.setRuleAt(i, r);
            initialization.init(neighbour);
            if (neighbour.getRuleAt(i) != solution.getRuleAt(i)) {
                neighbour.setFitness(automate.f(solution.getRules(), sizeMax));
                if (neighbour.fitness > solution.fitness) {
                    neighbour.copySolution(solution);
                    equal = false;
                }
            }
        }


        //System.out.println(neighbour.toString());
        solution.copySolution(neighbour);
    }
}
