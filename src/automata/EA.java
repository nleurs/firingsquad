package automata;

import java.util.Comparator;

public class EA {
    private static final int SELECTION_SIZE = 2;
    private static final int NB_PARENTS_CROSSOVER = 2;
    private static final Comparator<Solution> COMPARATOR_REPLACEMENT = Comparator.comparingInt(Solution::getFitness);

    /**
     * Initialise la population aléatoirement
     * @param population
     */
    public void init(Population population) {
        Initialization initialization = new Initialization();
        for(Solution sol : population) {
            initialization.init(sol);
        }
    }

    /**
     * Selectionne aléatoirement des elements de la population
     * pour les mettre dans la listes de geniteurs
     * @param population
     * @param geniteurs
     */
    public void select(Population population, Population geniteurs) {
        geniteurs.removeAllElements();
        for(int i = 0; i< SELECTION_SIZE; i++) {
            geniteurs.add(population.get((int)(Math.random()*population.size())));
        }
    }

    /**
     * Methode de variation de la population, crossover ou permutation, etc
     * @param population
     */
    public void variation(Population population) {
        crossover(population);
    }

    /**
     * Methode de crossover aleatoire parmi les membres de la population
     * @param population
     */
    public void crossover(Population population) {
        int frs = Initialization.freeRules.size();
        int index = frs/2;
        int tmp;
        for(int i=1; i<population.size(); i+=2) {
            for(int j=index; j<frs; j++) {
                tmp = population.get(i).getRuleAt(j);
                population.get(i).setRuleAt(j, population.get(i-1).getRuleAt(j));
                population.get(i-1).setRuleAt(j, tmp);
            }
        }
    }

    public void remplacement(Population parents, Population geniteurs) {
        parents.addAll(geniteurs);
        geniteurs.sort(COMPARATOR_REPLACEMENT);
    }
}
